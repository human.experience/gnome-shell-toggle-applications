/* ------------------------------------------------------------------------- */
// eslint configuration for this file
//
/* global imports */
/* global window */


/* exported init */
/* exported disable */
/* exported enable */


/* ------------------------------------------------------------------------- */
// enforce strict mode
"use strict";


/* ------------------------------------------------------------------------- */
// enable global used for debugging
window.toggleApplications = {
  debug: false,
};


/* ------------------------------------------------------------------------- */
// gnome shell imports
const ExtensionUtils = imports.misc.extensionUtils;


/* ------------------------------------------------------------------------- */
// extension imports
const Extension = ExtensionUtils.getCurrentExtension();
const ToggleApplications = Extension.imports.lib.toggleApplications;


/* ------------------------------------------------------------------------- */
// extension globals
var toggleApplicationsInstance;


/* ------------------------------------------------------------------------- */
function init() {
  toggleApplicationsInstance = null;
}


/* ------------------------------------------------------------------------- */
function enable() {
  toggleApplicationsInstance = new ToggleApplications.ToggleApplications();
  toggleApplicationsInstance.enable();
}


/* ------------------------------------------------------------------------- */
function disable() {
  toggleApplicationsInstance.disable();
  toggleApplicationsInstance = null;
}
