// ------------------------------------------------------------------------- //
// eslint configuration for this file
//
/* global window */

/* exported Logger */


// ------------------------------------------------------------------------- //
// enforce strict mode
"use strict";


// ------------------------------------------------------------------------- //
var Logger = class Logger {

  // ....................................................................... //
  constructor(moduleName) {
    this.applicationName = "[toggle-applications]";
    this.moduleName = moduleName;
  }

  // ....................................................................... //
  debug(message) {
    if (window.toggleApplications.debug === true) {
      let msg = `[${this.applicationName}] > ${this.moduleName}: ${message}`;
      window.log(msg);
    }
  }

};
