/* ------------------------------------------------------------------------- */
// eslint configuration for this file
//
/* global imports */

/* exported ToggleApplications */


/* ------------------------------------------------------------------------- */
// enforce strict mode
"use strict";


/* ------------------------------------------------------------------------- */
// system libraries imports
const Gio = imports.gi.Gio;
const Meta = imports.gi.Meta;
const Shell = imports.gi.Shell;

/* ------------------------------------------------------------------------- */
// gnome shell imports
const Main = imports.ui.main;
const ExtensionUtils = imports.misc.extensionUtils;
const Config = imports.misc.config;

/* ------------------------------------------------------------------------- */
// extension imports
const Extension = ExtensionUtils.getCurrentExtension();
const Utils = Extension.imports.lib.utils;

/* ------------------------------------------------------------------------- */
// extensions constants
const SHELL_KEYBINDINGS_SCHEMA = "org.gnome.shell.keybindings";


/* ------------------------------------------------------------------------- */
var ToggleApplications = class ToggleApplications {

  constructor() {

    // logger
    this._logger = new Utils.Logger(
      "applicationsToggle.js::OverviewButton"
    );

  }

  /* ....................................................................... */
  enable() {
    let keybindingFlags = this._keybindingFlagsForGnomeVersion();

    // remove existing keybinding handler
    Main.wm.removeKeybinding("toggle-application-view");

    // add our own keybinging handler
    Main.wm.addKeybinding(
      "toggle-application-view",
      new Gio.Settings({ schema_id: SHELL_KEYBINDINGS_SCHEMA }),
      keybindingFlags,
      Shell.ActionMode.NORMAL |
      Shell.ActionMode.OVERVIEW,
      this._toggleApplicationsViewHandler.bind(Main.overview)
    );
  }


  /* ....................................................................... */
  disable() {
    let keybindingFlags = this._keybindingFlagsForGnomeVersion();

    // remove our own keybinding handler
    Main.wm.removeKeybinding("toggle-application-view");

    // recreate the original keybinding
    Main.wm.addKeybinding(
      "toggle-application-view",
      new Gio.Settings({ schema_id: SHELL_KEYBINDINGS_SCHEMA }),
      keybindingFlags,
      Shell.ActionMode.NORMAL |
      Shell.ActionMode.OVERVIEW,
      Main.overview.viewSelector._toggleAppsPage.bind(
        Main.overview.viewSelector
      )
    );
  }

  /* ....................................................................... */
  _toggleApplicationsViewHandler() {
    if (Main.overview._shown === true) {
      Main.overview.hide();
    }
    else {
      Main.overview.viewSelector.showApps();
    }
  }

  /* ....................................................................... */
  _keybindingFlagsForGnomeVersion() {
    let version;
    let keybindingFlags;

    // get gnome shell version as a array
    version = Config.PACKAGE_VERSION.split(".");

    // if version is less then 3.30 the keybinding flags are NONE
    if (version[0] >= 3 && version[1] < 30) {
      keybindingFlags = Meta.KeyBindingFlags.NONE;
    }
    else {
      keybindingFlags = Meta.KeyBindingFlags.IGNORE_AUTOREPEAT;
    }

    return keybindingFlags;
  }

};
